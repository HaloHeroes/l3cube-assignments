#include<stdio.h>
#include<string.h>
#define max 50
#define max1 100

int count(){	//Keep the count of the existing files in file system in superblock
	FILE *fp;
	int count=0;
	char s[max1];
	fp=fopen("textfs.txt","r");
	while(fgets(s,max1,fp)!=NULL){
		if(strstr(s,"INODE-start")!=NULL){
			fgets(s,max1,fp);
			while(strstr(s,"INODE-end")==NULL){
				count++;
				fgets(s,max1,fp);
			}
		}
	}
	fclose(fp);
	return count;
}


void list_files(){	//list all the existing files in file system
	FILE *fp;
	char s[max1];
	int counter=count();
	fp=fopen("textfs.txt","r");
	while(fgets(s,max1,fp)!=NULL){
		if(strstr(s,"INODE-start")!=NULL){
			fgets(s,max1,fp);
			while(strstr(s,"INODE-end")==NULL){
				printf("%s\n",strtok(s," "));
				fgets(s,max1,fp);
			}
		}
	}
	fclose(fp);
	if(counter==0)	//empty file system with no files
		printf("\nNO FILES TO SHOW\n");
}


void create_file(char f[max]){ //creating new file in the file system
	FILE *fp1,*fp2;
	char s[max],*s1;
	int k=0,c;
	c=count()+1;
	fp1=fopen("textfs.txt","r");
	while(fgets(s,max1,fp1)!=NULL){
		s1=strtok(s,"\n");
		if(strcmp(s1,f)==0)
			k=1;
	}
	fclose(fp1);
	if(k==0){
		fp1=fopen("textfs.txt","r");
		fp2=fopen("temporary.txt","w");
		while(fgets(s,max1,fp1)!=NULL){
			if(strstr(s,"SUPERBLOCK-start")){
				fputs(s,fp2);
				fgets(s,max1,fp1);
				strcpy(s1,strtok(s,":"));
				strcat(s1,":-");
				fputs(s1,fp2);
				fprintf(fp2,"%d",c);
				fputs("\n",fp2);
			}
			else if(strstr(s,"INODE-end")!=NULL){
				strcpy(s1,f);
				fputs(s1,fp2);
				fputs("\nINODE-end\n",fp2);
			}
			else{
				s1=strtok(s,"\n");
				fputs(s1,fp2);
				fputs("\n",fp2);
			}
		}
		fclose(fp1);
		fclose(fp2);
		remove("textfs.txt");
		rename("temporary.txt","textfs.txt");
		fp1=fopen("textfs.txt","a+");
		s1=strtok(f," ");
		fputs(s1,fp1);
		fputs("-start\n",fp1);
		fputs(s1,fp1);
		fputs("-end\n",fp1);
		fclose(fp1);
	}
	else	//validation to check if file already exists
		printf("\nFILE ALREADY EXISTS\n");
}

void copy_file(char source[max],char destination[max]){ //copying contents of any external file to existing file
	FILE *fp1,*fp2,*fp3;
	char s[max],s1[max],s2[max];
	int k=0;
	fp1=fopen("textfs.txt","r");
	while(fgets(s,max1,fp1)!=NULL){
		strtok(s," ");
		if(strcmp(s,destination)==0)
			k=1;
	}
	fclose(fp1);
	if(k==1){
		if(fp1=fopen(source,"r")){
			fp2=fopen("textfs.txt","r");
			fp3=fopen("temporary.txt","w");
			strcpy(s1,destination);
			strcat(s1,"-start");
			while(fgets(s,max1,fp2)!=NULL){
			strtok(s,"\n");
			strtok(s1," ");
				if(strstr(s1,s)==NULL){
					fputs(s,fp3);
					fputs("\n",fp3);
				}
				else{
					fputs(s,fp3);
					fputs("\n",fp3);
					while(fgets(s,max1,fp1)!=NULL){
						fputs(s,fp3);
					}
				}
			}
			fclose(fp3);
			fclose(fp2);
			fclose(fp1);
			remove("textfs.txt");
			rename("temporary.txt","textfs.txt");
		}
		else
			printf("\nINVALID SOURCE FILE\n");
	}
	else
		printf("\nINVALID DESTINATION FILE\n");
}


void echo_file(char source[max]){ 	//printing the contents of any existing file in file system
	FILE *fp;
	char s[max],s1[max],*s2;
	int k=0;
	fp=fopen("textfs.txt","r");
	while(fgets(s,max1,fp)!=NULL){
		s2=strtok(s," ");
		if(strcmp(s2,source)==0)
			k=1;
	}
	fclose(fp);
	if(k==1){
		fp=fopen("textfs.txt","r");
		strcpy(s1,source);
		strcat(s1,"-start");
		while(fgets(s,max,fp)!=NULL){
			if(strstr(s,s1)!=NULL){
				fgets(s,max,fp);
				strcpy(s1,source);
				strcat(s1,"-end");
				while(strstr(s,s1)==NULL){
					printf("%s",s);
					fgets(s,max,fp);
				}
				break;
			}
		}
		fclose(fp);
	}
	else
		printf("\nINVALID FILENAME\n");
}


void delete_file(char source[max1]){	//deleting any existing file
	FILE *fp1,*fp2;
	char s[max1],s1[max1],*t,s2[max],s3[max],s4[max];
	int k=0,c;
	c=count()-1;
	fp1=fopen("textfs.txt","r");
	while(fgets(s,max1,fp1)!=NULL){
		t=strtok(s," ");
		if(strcmp(t,source)==0)
			k=1;
	}
	fclose(fp1);
	if(k==1){
		fp1=fopen("textfs.txt","r");
		fp2=fopen("temporary.txt","w");
		strcpy(s3,source);
		strcat(s3,"-start");
		strcpy(s2,source);
		strcat(s2,"-end");
		strcpy(s4,source);
		strcat(s4," \n");
		while(fgets(s,max,fp1)!=NULL){
			if(strstr(s,"SUPERBLOCK-start")!=NULL){
				fputs(s,fp2);
				fgets(s,max1,fp1);
				strcpy(s1,strtok(s,":"));
				strcat(s1,":-");
				fputs(s1,fp2);
				fprintf(fp2,"%d",c);
				fputs("\n",fp2);
			}
			else if(strstr(s,s3)!=NULL){
				fgets(s,max,fp1);
				while(strstr(s,s2)==NULL){
					fgets(s,max1,fp1);
				}
			}
			else if(strcmp(s,s4)!=0){
				fputs(s,fp2);
			}
			else
				continue;

		}
		fclose(fp1);
		fclose(fp2);
		remove("textfs.txt");
		rename("temporary.txt","textfs.txt");
	}
	else
		printf("\nINVALID FILENAME\n");
}


int main(){
	char *c,*f,*s,t[max],*f1;
	int d=0,counter;
	FILE *fp;
	//Initialization of textfs file
	if(fp=fopen("textfs.txt","r")){
		fclose(fp);
	}
	else{
		fp=fopen("textfs.txt","w");
		fputs("SUPERBLOCK-start\nNUMBER OF FILES:-",fp);
		fprintf(fp,"%d",0);
		fputs("\nNAME OF FILE SYSTEM:-textfs\nSUPERBLOCK-end\nINODE-start\nINODE-end\n",fp);
		fclose(fp);
	}
	do{
		printf("\ntextfs:~$ ");
		scanf("%[^\n]s",t);		//Input command
		c=strtok(t," ");
		if(strcmp(c,"ls")==0){		//Listing existing files in the file system
			list_files();
		}
		else if(strstr(c,"create")!=NULL){	//Creating new file in file system
			f=strtok(NULL," ");
			strcat(f," ");
			if(f!=NULL && strtok(NULL," ")==NULL){
				if(strstr(f,".txt ")!=NULL)
					create_file(f);
				else
					printf("\nONLY .txt FILES ARE ALLOWED\n");
			}
			else
				printf("\nINVALID OPERATION\nSYNTAX:- create <filename>\n");

			}
		else if(strstr(c,"echo")!=NULL){	//Printing the contents of any existing file
			s=strtok(NULL," ");
			if(s!=NULL && strtok(NULL," ")==NULL)
				echo_file(s);
			else
				printf("\nINVALID OPERATION\nSYNTAX:- echo <filename>\n");
		}
		else if(strstr(c,"delete")!=NULL){	//Deleting any existing file
			s=strtok(NULL," ");
			if(s!=NULL && strtok(NULL," ")==NULL)
				delete_file(s);
			else
				printf("\nINVALID OPERATION\nSYNTAX:- delete <filename>\n");
		}
		else if(strcmp(c,"copy")==0){	//Copying the contents from external file to the file in file system
			s=strtok(NULL," ");
			f=strtok(NULL," ");
			if(s!=NULL && f!=NULL && strtok(NULL," ")==NULL)
				copy_file(s,f);
			else
				printf("\nINVALID OPERATION\nSYNTAX:- copy <source> <destination>\n");
		}
		else if(strcmp(c,"quit")==0){
			d=1;
		}
		else
		printf("Supported Commands:-\nls\ncreate <filename>\necho <filename>\ndelete <filename>\ncopy <source> <destination>\nquit\n");
		getchar();
	}while(d!=1);
}
