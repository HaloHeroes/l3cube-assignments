Use GCC.
> gcc textfs.c -o textfs
> ./textfs

The command interpreter accepts only five commands: create, ls, echo, delete and copy. The supported commands are prompted as soon as the invalid key is pressed.
Every command is simple.
For copy, copy <the path of the source file><space><destination file created using our file system>
For delete,  remove <filename>
For list, ls
For create, create <filename>
For printing, print <filename>

textfs.txt contains the metadata of the filesystem.