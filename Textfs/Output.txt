shweta:Textfs$ gcc textfs.c -o textfs
shweta:Textfs$ ./textfs
textfs:~$ p
Supported Commands:-
ls
create <filename>
echo <filename>
delete <filename>
copy <source> <destination>
quit

textfs:~$ ls

NO FILES TO SHOW

textfs:~$ create abc

ONLY .txt FILES ARE ALLOWED

textfs:~$ create abc.txt

textfs:~$ create xyz.txt

textfs:~$ ls
abc.txt
xyz.txt

textfs:~$ copy /home/shweta/a.txt abc.txt

textfs:~$ echo abc.txt
Hello
This is just a trial.
Hope this works.

textfs:~$ copy /home/shweta/Desktop/b.txt xyz.txt

textfs:~$ echo xyz.txt
Well, this is working! Yayy!

textfs:~$ ls
abc.txt
xyz.txt

textfs:~$ delete abc.txt

textfs:~$ ls
xyz.txt

textfs:~$
