 *  Make sure to compile with -o svc as an extra parameter.
 *  Usage: svc filename to commit
 *  	   svc filename N to output nth version of file
 *  Operations permitted:
 *                        Appending a new line at the end of the file.
                          Deleting any existing line.
 *  Only one of the above operations must be performed at one time.
 *  This program can control versions for multiple files.
 *  The changelog file for every file is named .changelog_<filename>.txt.
 *  The changelog file geneated by program is hidden for security issues. to view them in file explorer use ctrl+h (ubuntu)
 *  The changelog file has following format:
 *  vN
 *  a (string) or rM
 *  e
 *  vN specifies the Nth version of file.a stands for append and r stands for remove. e signifies end of changes.
 
 Use GCC.
 > g++ svc.cpp -o svc
 >./svc text.txt
 
 Follow the procedure as mentioned in Output.txt and watch .changelog created when you run this program for versions.
 