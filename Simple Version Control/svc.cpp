/* 	Documentation:
 *  Make sure to compile with -o svc as an extra parameter.
 *  Usage: svc filename to commit
 *  	   	 svc filename N to output nth version of file
 *  Operations permitted:
 *                        Appending a new line at the end of the file.
													Deleting any existing line.
 *  Only one of the above operations must be performed at one time.
 *  This program can control versions for multiple files.
 *  The changelog file for every file is named .changelog_<filename>.txt.
 *  The changelog file geneated by program is hidden for security issues. to view them in file explorer use ctrl+h (ubuntu)
 *  The changelog file has following format:
 *  vN
 *  a (string) or rM
 *  e
 *  vN specifies the Nth version of file.a stands for append and r stands for remove. e signifies end of changes
 */
#include<iostream>
#include<fstream>
#include<string>
#include<cstdlib>
#include<sstream>
using namespace std;
class svc
{	int num;
	string fname;
public:
	int make_file(string file,string content[20],int &ver,int op=-1)//function to construct file from its changelog
	{	string str;
		int lines=0;
		int tmp=file.find(".txt");
		fname=".changelog_"+file.substr(0,tmp)+".txt";
		ifstream his(fname.c_str());
		if(!his.is_open())
		{
			return lines;
		}
		while(getline(his,str))
		{
			if(str[0]=='v')
			{	stringstream(str.substr(1))>>ver;
				continue;
			}
			if(str[0]=='e')
			{	if(op!=-1)
				{
					if(op==ver)
						return lines;

				}
				continue;
			}
			if(str[0]=='a')
			{
				content[lines++].assign(str.begin()+2,str.end());
			}
			if(str[0]=='r')
			{	int temp=str[1]-48;
				while(temp<lines)
				{	content[temp]=content[temp+1];
					temp++;
				}
				lines--;

			}
		}
		his.close();
		return lines;
	}
	int ret_num()
	{
		return num;
	}
	bool change_commit(string file,string content[],int lines,int ver)//function to commit changes made to the file
	{	int lno=0,flag=0,delf=0;
		ofstream his;
		ifstream f(file.c_str());
		string str;
		ostringstream oss;
		if(!f.is_open())
		{
			cout<<file<<" does not exist";
			exit(1);
		}
		his.open(fname.c_str(),ios::app);
		while(getline(f,str))
		{	if(str.length()>10) // Each line has a maximum character width of 10 characters
			{	cout<<"Line width overflow, only 10 characters allowed!\n";
				exit(0);
			}
			if(lno<lines)
			{
				if(str.compare(content[lno])!=0)
				{
					oss<<"r"<<lno<<"\n";
					flag=1;
					delf=1;
					break;
				}
			}
			else
			{
				oss<<"a "<<str<<"\n";
				flag=1;
			}
			lno++;
		}
		if(lno<lines&&delf==0)
		{	oss<<"r"<<lno<<"\n";
			flag=1;
		}
		f.close();
		if(flag==0)
		{	return false;
			his.close();
		}
		his<<"v"<<ver<<"\n";
		his<<oss.str();
		his<<"e\n";
		his.flush();
		his.close();
		return true;
	}
	bool is_num(char c[])
	{
		for(int i=0;c[i]!=0;i++)
			if(!isdigit(c[i]))
				return false;
		num=0;
		for(int i=0;c[i]!=0;i++)
			num=num*10+(c[i]-48);
		return true;
	}


};
int main(int argc, char * argv[])
{	if(argc<2||argc>3)
	{	cout<<"Invalid number of parameters. Check documentation for usage\n";
		return 0;
	}
	string file(argv[1]),content[20]; //The total number of lines is 20
	int version=-1,reqv,l;
	svc s;
	if(argc==2)//if input is svc filename
	{
		l=s.make_file(file,content,version);
		version++;
		bool b=s.change_commit(file,content,l,version);
		if(b)
			cout<<"Changes committed.\n";
		else
			cout<<"No changes found.\n";
	}

	else//if input is svc filename N
	{
		bool b=s.is_num(argv[2]);
		if(!b)
		{	cout<<"Enter a valid integer!\n";
			exit(1);
		}
		fstream f(argv[1]);
		if(!f.is_open())
		{	cout<<argv[1]<<" does not exist!\n";
			exit(1);
		}
		f.close();
		reqv=s.ret_num();
		l=s.make_file(file,content,version,reqv);
		if(reqv>version)
			cout<<"There is no version "<<reqv<<", latest version is version "<<version<<":\n";
		for(int i=0;i<l;i++)
			cout<<content[i]<<"\n";
	}
	return 0;
}
