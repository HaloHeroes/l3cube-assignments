#include<stdio.h>

int main(){
	int inp[400], i, c, j, k, l, p, try=100000;
	double d;
	printf("\nBIRTHDAY PARADOX PROBLEM:-\n");
	printf("It defines that the probability of birthday on same day among any two people is 0.50 when number of people is 23,");
	printf("and is 0.999 when number of people reaches to 70\n");
	printf("\n100000 test cases for different number of people in group is carried out, to give probabilities as follows: \n");
	printf("\nNumber of people: Probability\n");

	for(i = 1; i <= 70; i++){ //Total number of people is considered to be 70
		p = 0;
		for(j = 1; j <= try; j++){ //Number of test cases = 100000
			for(k = 1; k <= i; k++){
				inp[k] = rand() % 366;
			}
			c=0;
			for(k = 1; k <= i && c == 0; k++){ //Number of comparisons
				for(l = k+1; l <= i && c == 0; l++){
					if(inp[k] == inp[l] && k != l)
						c++;
				}
			}
			if(c!=0)
				p=p+1;
		}
		d=(double)p/try;
		printf("\n\t%d\t: %lf", i, d);
	}
	printf("\nThus, we verified the birthday paradox problem, as the probabilities at 23 and 70 are 0.50 and 0.999 approximately.\n");
}
