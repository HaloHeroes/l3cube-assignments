Enter the directory name that needs to be checked for duplication.
Here to check for complete hard-drive, use ., else enter the directory name with its path.
For example, in this folder, we have Trial as a folder which contains duplicate files in its sub-directories. The output is as shown in the output.txt file.

Use GCC.
> gcc duplicate_files.c -o duplicate_files
> ./duplicate_files
Follow the commands. If the duplicate files are to be searched in the whole hard disk, press "." and enter.
Else, enter the path of the directory to be searched for duplication.
Here, if we type Trial, we get the list of all the duplicate files.

rec.txt contains records of all text files.
dup_rec.txt contains records of all duplicate entries in a directory.

Same can be checked for hard disk too.