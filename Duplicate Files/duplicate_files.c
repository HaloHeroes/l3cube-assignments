#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<dirent.h>
#define max 1000

void operate(const char *m,const char *n){
	FILE *fp1,*fp2;
	char s[max],p[max];
	int f=0;
	fp1=fopen("rec.txt","a+");
	fp2=fopen("dup_rec.txt","a");
	while(fgets(s,max,fp1)!=NULL){
		strcpy(p,"~");
		strcat(p,n);
		strcat(p,"~");
		if(strstr(s,p)!=NULL){
			f=1;
		}
	}
	//first occurrence of the file
	if(f==0){
		strcpy(p,m);
		strcat(p,"~");
		strcat(p,n);
		strcat(p,"~");
		//copying filename with its path to rec.txt in the form path_of_the_file~filename~
		fputs(p,fp1);
		fputs("\n",fp1);
	}
	else{
		strcpy(p,m);
		strcat(p,"~");
		strcat(p,n);
		strcat(p,"~");
		//copying filename with its path to dup_rec.txt in the form path_of_the_file~filename~
		fputs(p,fp2);
		fputs("\n",fp2);
	}
	fclose(fp1);
	fclose(fp2);
}

void list(const char *n){
	static int a=0,b=0;
	if(a==0){
		printf("Please wait...scanning directory\n");
		a++;
	}
	DIR *dir;
	struct dirent *ent;
	char p[max];
	//opening the directory
	if ((dir = opendir (n)) != NULL) {
		b=1;
		// reading all the files and directories within directory
		while ((ent = readdir (dir)) != NULL) {
			strcpy(p,n);
			//checking for subdirectory
			if(ent->d_type==4 && strcmp(ent->d_name,".")!=0 && strcmp(ent->d_name,"..")!=0){
				strcat(p,"/");
				strcat(p,ent->d_name);
				list(p);	//to explore the subdirectory
			}
			//checking for file
			else if(ent->d_type==8){
				operate(p,ent->d_name);
			}
		}
	closedir (dir);
	}
	else if(b==0){
		printf("\nINVALID PATH\n");
		exit(0);
	}
}

int operations(){
  FILE *fp1,*fp2;
  char s[max],*s1;
  char f[max],f1[max],tp[max][max];
  int ch,i,op,ret,c,c1;
  do{
	printf("\n1.LIST DUPLICATE FILES\n2.DELETE ANY DUPLICATE FILE\n3.EXIT\nSELECT OPTION:-");
	scanf("%d",&ch);
	switch(ch){
		case 1:
			i=0;
			printf("\nLIST OF DUPLICATE FILES:-\n");
			fp1=fopen("dup_rec.txt","r");
			while(fgets(s,max,fp1)!=NULL){
				s1=strtok(s,"~");
				s1=strtok(NULL,"~");
				c1=0;
				if(i<max){ //all duplicate files will be displayed once if no. of such files is less than max
					i=i+1;
					strcpy(tp[i],s1);
					for(c=1;c<i;c++){
						if(strcmp(tp[i],tp[c])==0){
							c1=1;
							i=i-1;
						}
					}
					if(c1==0)
						printf("%s\n",s1);
				}
				else  //after max, files which are duplicated more than once will be displayed again if occurred
					printf("%s\n",s1);
			}
			fclose(fp1);
			if(i==0)
				printf("\nNO DUPLICATE FILES PRESENT IN THE DIRECTORY\n");
			break;
		case 2:
			i=0;
			printf("\nEnter the file to be deleted:-\n");
			scanf("%s",f);
			printf("\nDuplicates of file %s are:-\n",f);  //Displaying all the occurrences of the file to be deleted
			fp1=fopen("rec.txt","r");
			fp2=fopen("dup_rec.txt","r");
			strcpy(f1,"~");
			strcat(f1,f);
			strcat(f1,"~");
			while(fgets(s,max,fp2)!=NULL){
				if(strstr(s,f1)!=NULL){
					s1=strtok(s,"~");
					strcat(s1,"/");
					strcat(s1,f);
					i=i+1;
					printf("%d.%s\n",i,s1);
					strcpy(tp[i],s1);
				}
			}
			strcpy(f1,"~");
			strcat(f1,f);
			strcat(f1,"~");
			while(fgets(s,max,fp1)!=NULL){
				if(strstr(s,f1)!=NULL){
					s1=strtok(s,"~");
					strcat(s1,"/");
					strcat(s1,f);
					i=i+1;
					printf("%d.%s\n",i,s1);
					strcpy(tp[i],s1);
				}
			}
			fclose(fp2);
			fclose(fp1);
			op=i+1;
			while(op>i){
				printf("\nENTER THE INDEX OF THE FILE TO BE DELETED(0 for exit):-");
				scanf("%d",&op);
				if(op>i)
					printf("\nINVALID CHOICE\n");
				else if(op==0)
					break;
				else{
					ret=remove(tp[i]);
					if(ret==0){
						printf("File deleted successfully\n");
						return 1;
					}
					else{
						printf("Error: unable to delete the file\n");
					}
				}
			}
			break;
	}
  }while(ch!=3);
  return 0;
}


void main(){
  FILE *fp1,*fp2;
  char inp[100];
  printf("\nEnter the directory that need to be scanned for duplication(. for complete hard-drive) or path:\n");
  scanf("%s",inp);
  do{
	fp1=fopen("rec.txt","w");
	fp2=fopen("dup_rec.txt","w");
	fclose(fp1);
	fclose(fp2);
	if(strcmp(inp,".")==0)
		list("."); 	// "." for duplication in complete hard-drive
	else
		list(inp);	//for any other directory; Trial is a subdirectory for testing.
  }while(operations()==1);
}